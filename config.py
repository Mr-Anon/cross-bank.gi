import pygame
pygame.init()
# resolution
screen = pygame.display.set_mode((1600, 900))
#  name
pygame.display.set_caption("CROSSBANK")
# icon
icon = pygame.image.load('icon.png')
pygame.display.set_icon(icon)
player1 = pygame.image.load('player.png')
playerImg = player1
player2 = pygame.image.load('player2.png')
playerX = 760
playerY = 840
playerX_change = 0
playerY_change = 0

flag = 0
count=0
enemyImg = []
enemyX = []
enemyY = []
enemyX_change = []
enemyY_change = []
num_of_enemies = 11
# color
backgroundColor =(77, 190, 255)
stripcolor=(252, 208, 70)

score_valuep1 = 0
prevsp1 = 0
prevsp2 = 0
sp1 = 0
sp2 = 0
r22 = 0
r21 = 0
score_valuep2 = 0
font = pygame.font.Font('freesansbold.ttf', 32)

winner_font = pygame.font.Font('freesansbold.ttf', 64)

t1p1 = 0
t2p1 = 0
t1p2 = 0
t2p2 = 0
r1p1w = False
r1p2w = False

for i in range(num_of_enemies):
    if i < 5:
        enemyImg.append(pygame.image.load('enemy.png'))
        if i == 0:
            enemyX.append(100)
            enemyY.append(80)
        else:
            enemyX.append(250 + enemyX[i - 1])
            enemyY.append(166 + enemyY[i - 1])
    else:
        enemyImg.append(pygame.image.load('enemy2.png'))
        if i == 5:
            enemyX.append(10)
            enemyY.append(5)
        else:
            enemyX.append(250 + enemyX[i - 1])
            enemyY.append(166 + enemyY[i - 1])
