import math
from config import *

# end text
def game_end_text():
    if sp1 > sp2:
        winner_text1 = winner_font.render("Player 1 wins", True, (255, 255, 255))
        screen.blit(winner_text1, (200, 250))
        pygame.display.update()
        pygame.time.wait(200)
    elif sp2 > sp1:
        winner_text2 = winner_font.render("Player 2 wins", True, (255, 255, 255))
        screen.blit(winner_text2, (200, 250))
        pygame.display.update()
        pygame.time.wait(200)
    else:
        winner_text3 = winner_font.render("TIE", True, (255, 255, 255))
        screen.blit(winner_text3, (200, 250))
        pygame.display.update()
        pygame.time.wait(200)



# score for palyer 1
def show_scorep1(x, y):
    global sp1
    if count == 0:
        scorep1 = font.render("Score : " + str((score_valuep1 - t1p1)), True, (255, 255, 255))
        assert isinstance(screen, object)
        screen.blit(scorep1, (x, y))
    else:
        scorep1 = font.render("Score : " + str((r21 + prevsp1 - t2p1 - t1p1)), True, (255, 255, 255))
        assert isinstance(screen, object)
        screen.blit(scorep1, (x, y))
    sp1 = r21 + prevsp1 - t2p1 - t1p1

# score for player 2
def show_scorep2(x, y):
    global sp2
    if count == 1:
        scorep2 = font.render("Score : " + str((score_valuep2 - t1p2)), True, (255, 255, 255))
        assert isinstance(screen, object)
        screen.blit(scorep2, (x, y))
    else:
        scorep2 = font.render("Score : " + str((r22 + prevsp2 - t2p2 - t1p2)), True, (255, 255, 255))
        assert isinstance(screen, object)
        screen.blit(scorep2, (x, y))
    sp2 = r22 + prevsp2 - t2p2 - t1p2

#  collision
def isCollision(enemyX, enemyY, playerX, playerY):
    distance = math.sqrt(math.pow(enemyX - playerX, 2) + (math.pow(enemyY - playerY, 2)))
    if distance < 64:
        return True
    else:
        return False

# for enemeny
def enemy(x, y, i):
    screen.blit(enemyImg[i], (x, y))

# for enemy
def enemy2(x, y, i):
    screen.blit(enemyImg[i], (x, y))

# for player
def player(x, y):
    screen.blit(playerImg, (x, y))


running = True
while running:
    screen.fill(backgroundColor)
    pygame.draw.rect(screen, stripcolor, (0, 0, 1600, 70))
    pygame.draw.rect(screen, stripcolor, (0, 166, 1600, 70))
    pygame.draw.rect(screen, stripcolor, (0, 332, 1600, 70))
    pygame.draw.rect(screen, stripcolor, (0, 498, 1600, 70))
    pygame.draw.rect(screen, stripcolor, (0, 664, 1600, 70))
    pygame.draw.rect(screen, stripcolor, (0, 830, 1600, 70))

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                playerX_change = -1
            if event.key == pygame.K_RIGHT:
                playerX_change = 1
            if event.key == pygame.K_UP:
                playerY_change = -1
            if event.key == pygame.K_DOWN:
                playerY_change = 1
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                playerX_change = 0
            if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                playerY_change = 0
    if playerX <= 0:
        playerX = 0
    if playerX >= 1536:
        playerX = 1536
    if playerY <= 0:
        playerY = 0
        if flag == 0:
            flag = 1
            count += 1
            playerImg = player2
            r1p1w = True
    if playerY >= 846:
        playerY = 846
        if flag == 1:
            flag = 0
            count += 1
            playerImg = player1
            r1p2w = True

    for i in range(num_of_enemies):
        if i < 5:
            if count < 2:
                enemy(enemyX[i], enemyY[i], i)
                enemyX[i] += 4
            elif r1p1w and flag == 0:
                enemy(enemyX[i], enemyY[i], i)
                enemyX[i] += 7
            elif r1p2w and flag == 1:
                enemy(enemyX[i], enemyY[i], i)
                enemyX[i] += 7
            else:
                enemy(enemyX[i], enemyY[i], i)
                enemyX[i] += 4

            if enemyX[i] >= 1546:
                enemyX[i] = 0
        else:
            enemy2(enemyX[i], enemyY[i], i)
        collision = isCollision(enemyX[i], enemyY[i], playerX, playerY)
        if collision:

            if flag == 0:
                playerY = 0
                count += 1
                flag = 1
                playerX = 760
                playerImg = player2
            else:
                playerY = 840
                count += 1
                flag = 0
                playerImg = player1
                playerX = 760
        if flag == 0:
            score_valuep2 = 0
            if playerY == enemyY[i]:
                if i == 0:
                    score_valuep1 = 75
                if i == 1:
                    score_valuep1 = 60
                if i == 2:
                    score_valuep1 = 45
                if i == 3:
                    score_valuep1 = 30
                if i == 4:
                    score_valuep1 = 15
                if i == 5:
                    score_valuep1 = 80
                if i == 6:
                    score_valuep1 = 65
                if i == 7:
                    score_valuep1 = 50
                if i == 8:
                    score_valuep1 = 35
                if i == 9:
                    score_valuep1 = 20
                if i == 10:
                    score_valuep1 = 5
                r21 = score_valuep1

        if flag == 1:
            score_valuep1 = 0
            if playerY == enemyY[i]:
                if i == 0:
                    score_valuep2 = 15
                if i == 1:
                    score_valuep2 = 30
                if i == 2:
                    score_valuep2 = 45
                if i == 3:
                    score_valuep2 = 60
                if i == 4:
                    score_valuep2 = 75
                if i == 5:
                    score_valuep2 = 5
                if i == 6:
                    score_valuep2 = 20
                if i == 7:
                    score_valuep2 = 35
                if i == 8:
                    score_valuep2 = 50
                if i == 9:
                    score_valuep2 = 65
                if i == 10:
                    score_valuep2 = 80
            r22 = score_valuep2

        if count == 0:
            prevsp1 = score_valuep1
            t1p1 = (int)((pygame.time.get_ticks()) / 1000)
        if count == 1:
            prevsp2 = score_valuep2
            r21 = 0
            t1p2 = (int)((pygame.time.get_ticks()) / 1000 - t1p1)
        if count == 2:
            r22 = 0
            t2p1 = (int)((pygame.time.get_ticks()) / 1000 - t1p2 - t1p1)
        if count == 3:
            t2p2 = (int)((pygame.time.get_ticks()) / 1000 - t2p1 - t1p2 - t1p1)
        if count == 4:
            game_end_text()
            running = False
    # print score
    show_scorep1(10, 10)
    # print score
    show_scorep2(1425, 850)
    #increment
    playerX += playerX_change
    #increment
    playerY += playerY_change
    player(playerX, playerY)
    # update display
    pygame.display.update()

